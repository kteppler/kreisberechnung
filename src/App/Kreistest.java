package App;

import java.util.Scanner;
import Model.Kreis;

public class Kreistest {
	private Scanner scanner = new Scanner(System.in);
	private Kreis kreis;
	private double umfang;
	private double flaeche;
	private double radius;
	
	public static void main(String[] args) {
		new Kreistest();
	}
	
	//Konstruktor
	private Kreistest() {
		System.out.println("Kreistest [Programm Start]");
		boolean weiter = true;
		while(weiter) {
			//Menu1
			System.out.print("Kreistest [1=Eingabe, 2=Verarbeitung, 3=Ausgabe, 4=Ende] > ");
			int auswahl = scanner.nextInt();
			if(auswahl==1)gibEin();
			if(auswahl==2 && radius>0)verarbeite();
			if(auswahl==3)gibAus();
			else System.out.println("Kreistest[falschse Eingabe]");
			if(auswahl==4)weiter=false;
		}
		scanner.close();
		System.out.println("Kreistest [Programm Ende]");
	}
	//Eingabe
	private void gibEin() {
	    kreis = new Kreis();
	    System.out.print("Kreistest [Geben Sie den Radius in cm ein] >");
	    radius = scanner.nextDouble();
		kreis.setRadius(radius);
	}
	//Verabreitung
	private void verarbeite() {
		umfang = kreis.getUmfang();
		flaeche = kreis.getFlaeche();
	}
	//Ausgabe
	private void gibAus() {
		System.out.println("Kreisberechnung [Fl�che: "+flaeche+"cm^2 Umfang: "+umfang+" cm]>");
	}


}
