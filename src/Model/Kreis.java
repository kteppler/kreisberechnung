package Model;

public class Kreis {
	private double radius;
	
	
	public Kreis() {
	}
	
	public Kreis(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getUmfang() {
		return 2 * Math.PI * radius;
	}
	
	public double getFlaeche() {
		return Math.PI * Math.pow( radius,2 );
	}
	
	public double getDurchmesser() {
		return 2*radius;
	}
}
